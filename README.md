# Advent of Code

My solution of [Advent of Code](https://adventofcode.com).

To run the script:

```bash
python -m <year>.<day>
```

e.g.

```bash
python -m 2022.01
```
