from pathlib import Path
from typing import Iterator


def read_file(path: Path) -> Iterator[str]:
    with open(path) as f:
        for line in f:
            yield line.rstrip()


def write_file(obj, path: Path):
    with open(path, 'w') as f:
        if isinstance(obj, list):
            for line in obj:
                f.write(f'{line}\n')
