# task: https://adventofcode.com/2022/day/7
from pathlib import Path
from typing import Tuple

from utils.files import read_file


def get_subtree(tree, path):
    for segment in path:
        tree = tree[segment]
    return tree


def read_tree(path):
    tree = {'/': {}}
    curr_path = []
    for line in read_file(path):
        if line.startswith('$'):
            command = line[2:]
            if command.startswith('cd'):
                directory = command[3:]
                if directory == '..':
                    curr_path.pop()
                else:
                    curr_path.append(directory)
                # print(curr_path)
        else:
            curr_subtree = get_subtree(tree, curr_path)
            if line.startswith('dir'):
                curr_subtree[line[4:]] = {}
            else:
                size, file = line.split()
                curr_subtree[file] = int(size)

    return tree


def sum_small_dirs(tree, max_dir_size: int) -> Tuple[int, int]:
    size = 0
    size_small = 0
    for k, v in tree.items():
        if isinstance(v, int):
            size += v
        elif isinstance(v, dict):
            dir_size, dir_size_small = sum_small_dirs(v, max_dir_size)
            size += dir_size
            size_small += dir_size_small

    if size <= max_dir_size:
        size_small += size

    return size, size_small


def find_dir_to_delete(tree, min_dir_size: int) -> Tuple[int, int]:
    size = 0
    size_to_delete = float('inf')
    for k, v in tree.items():
        if isinstance(v, int):
            size += v
        elif isinstance(v, dict):
            dir_size, dir_size_to_delete = find_dir_to_delete(v, min_dir_size)
            size += dir_size
            size_to_delete = min(size_to_delete, dir_size_to_delete)

    if min_dir_size <= size < size_to_delete:
        size_to_delete = size

    return size, size_to_delete


def main():
    curr_directory = Path(__file__).parent
    tree = read_tree(curr_directory/'input')

    # task 1
    size, size_small = sum_small_dirs(tree, max_dir_size=100000)
    print(size_small)

    # task 2
    max_used_space = 40000000
    min_dir_size = size - max_used_space
    _, size_to_delete = find_dir_to_delete(tree, min_dir_size=min_dir_size)
    print(size_to_delete)


if __name__ == '__main__':
    main()
