# task: https://adventofcode.com/2022/day/10
from pathlib import Path

import numpy as np

from utils.files import read_file


def main():
    # task 1
    x = 1
    cycles = [x]

    for line in read_file(Path(__file__).parent/'input'):
        if line == 'noop':
            cycles += [x]
        else:
            cycles += 2 * [x]
            x += int(line.split()[-1])

    signal_strength = sum(i * cycles[i] for i in range(20, 221, 40))

    print(signal_strength)

    # task 2
    screen = []
    width = 40
    height = 6
    for i, x in enumerate(cycles[1:]):
        screen.append('#' if i % width in (x - 1, x, x + 1) else '.')

    for i in range(height):
        offset = i * width
        print(''.join(screen[offset:offset + width]))


if __name__ == '__main__':
    main()
