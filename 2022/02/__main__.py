# assignment: https://adventofcode.com/2022/day/2
from pathlib import Path

from utils.files import read_file

weapons = [
    'A',  # rock
    'B',  # paper
    'C',  # scissors
]

weapon_scores = {
    'A': 1,
    'B': 2,
    'C': 3,
}

weapon_codes = {
    'X': 'A',
    'Y': 'B',
    'Z': 'C',
}


def get_your_weapon(opponent: str, result: str) -> str:
    """ Choose your weapon based on the opponent's weapon and expected result.

    Choose:
        * a weapon with the previous index (module) from weapons list to lose
        * the same weapon to have a draw
        * a weapon with the next index (module) from weapons list to win

    Args:
        opponent: opponent's weapon, one of ['A', 'B', 'C'] for rock, paper and scissors
        result: expected result, one of ['X', 'Y', 'Z'] for lose, draw and win

    Returns:
        your weapon, one of ['A', 'B', 'C']
    """
    if result == 'X':
        # lose
        return weapons[(weapons.index(opponent) - 1) % len(weapons)]
    if result == 'Y':
        # draw
        return opponent
    # win
    return weapons[(weapons.index(opponent) + 1) % len(weapons)]


def calculate_outcome_score(opponent: str, you: str) -> int:
    """ Calculate the score of the outcome of the round based on the weapons used.

    Score of the outcomes:
        * lose: 0
        * draw: 3
        * win: 6

    Args:
        opponent: opponent's weapon, one of ['A', 'B', 'C'] for rock, paper, scissors
        you: your weapon, one of ['A', 'B', 'C'] for rock, paper, scissors

    Returns:
        round outcome score, one of [0, 3, 6]
    """
    if opponent == you:
        # draw
        return 3
    if (weapons.index(opponent) + 1) % len(weapons) == weapons.index(you):
        # you win
        return 6
    # lose
    return 0


def calculate_score(opponent: str, c2: str, c2_as_result: bool = False) -> int:
    """ Calculate the score of the round.

    score = outcome_score + weapon_score

    Args:
        opponent: opponent's weapon, one of ['A', 'B', 'C'] for rock, paper, scissors
        c2: second column value, either your weapon code or expected round outcome, one of ['X', 'Y', 'Z']
        c2_as_result: whether the second column represents the round outcome or your weapon code

    Returns:
        round score
    """
    you = get_your_weapon(opponent, c2) if c2_as_result else weapon_codes[c2]
    return calculate_outcome_score(opponent, you) + weapon_scores[you]


def main():
    total_score_1 = 0
    total_score_2 = 0
    for line in read_file(Path(__file__).parent/'input'):
        c1, c2 = line.split()
        # task 1
        score_1 = calculate_score(c1, c2)
        total_score_1 += score_1
        # task 2
        score_2 = calculate_score(c1, c2, c2_as_result=True)
        total_score_2 += score_2
        print(c1, c2, score_1, score_2)
    # task 1
    print('Total score for encoded weapons:', total_score_1)
    # task 2
    print('Total score for round outcome:', total_score_2)


if __name__ == '__main__':
    main()
