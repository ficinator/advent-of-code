# task: https://adventofcode.com/2022/day/14
import curses
from itertools import pairwise
from pathlib import Path

import numpy as np

from utils.coords import Coords
from utils.files import read_file


class Cave:
    margin_x = 200
    margin_y = 3

    def __init__(self, rock: np.ndarray, has_floor: bool = False):
        self.min_coords = np.min(rock, axis=0)
        self.max_coords = np.max(rock, axis=0)

        data_shape = (self.max_coords[0] + Cave.margin_x, self.max_coords[1] + Cave.margin_y)
        self.data = np.full(shape=data_shape, fill_value='.')
        self.data[rock[:, 0], rock[:, 1]] = '#'

        self.source: Coords = (500, 0)
        self.data[self.source[0], self.source[1]] = '+'

        self.has_floor = has_floor
        if self.has_floor:
            # add floor (for the 2nd task)
            self.data[:, -1] = '#'

    def __str__(self) -> str:
        return f'{self.min_coords}:{self.max_coords}, {self.data.shape}'

    @classmethod
    def from_file(cls, path, has_floor: bool = False) -> 'Cave':
        rock = []
        for line in read_file(path):
            coords_gen = (tuple(map(int, coords.split(','))) for coords in line.split(' -> '))
            wall = []
            for coords_start, coords_end in pairwise(coords_gen):
                if coords_start > coords_end:
                    coords_start, coords_end = coords_end, coords_start
                wall_segment = np.mgrid[coords_start[0]:coords_end[0] + 1, coords_start[1]:coords_end[1] + 1]
                wall.append(wall_segment.squeeze().T)
            rock.append(np.vstack(wall))

        return cls(rock=np.vstack(rock), has_floor=has_floor)

    def draw(self, stdscr):
        """ Draw the cave using curses library.

        IMPORTANT: make sure the terminal is tall enough or it throws an ERR
        """
        for y, row in enumerate(self.data[self.min_coords[0] - Cave.margin_x:, :].T):
            stdscr.addstr(y, 0, ''.join(row))
        stdscr.refresh()

    def produce_sand_unit(self, counter: int, stdscr=None) -> bool:
        x, y = self.source

        self.data[-7:, 0] = list(f'{counter:07d}')

        came_to_rest = False
        while not came_to_rest and (self.has_floor or y < self.max_coords[1]):
            if self.data[x, y + 1] == '.':
                y += 1
            elif self.data[x - 1, y + 1] == '.':
                x -= 1
                y += 1
            elif self.data[x + 1, y + 1] == '.':
                x += 1
                y += 1
            else:
                self.data[x, y] = 'o'
                came_to_rest = True

        if stdscr:
            self.draw(stdscr)

        return (x, y) != self.source if self.has_floor else came_to_rest


def main(stdscr=None, has_floor: bool = False):
    cave = Cave.from_file(Path(__file__).parent/'input', has_floor=has_floor)

    counter = 0
    while cave.produce_sand_unit(counter, stdscr):
        counter += 1

    if stdscr:
        # wait for a key from the user
        stdscr.getch()
    else:
        # for the case with the floor add one (the last) sand unit
        print(counter)


if __name__ == '__main__':
    # task 1
    curses.wrapper(main)
    # task 2
    main(has_floor=True)
