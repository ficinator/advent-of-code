# task: https://adventofcode.com/2022/day/13
import json
from functools import cmp_to_key
from pathlib import Path
from typing import List

import numpy as np

from utils.files import read_file, write_file

Packet = int | List['Packet']


def compare(packet_1: Packet, packet_2: Packet) -> int:
    if isinstance(packet_1, int) and isinstance(packet_2, int):
        if packet_1 > packet_2:
            return 1
        elif packet_1 < packet_2:
            return -1
        else:
            return 0

    if isinstance(packet_1, int):
        packet_1 = [packet_1]
    elif isinstance(packet_2, int):
        packet_2 = [packet_2]

    for item_1, item_2 in zip(packet_1, packet_2):
        item_comparison = compare(item_1, item_2)
        if item_comparison:
            return item_comparison

    length_1 = len(packet_1)
    length_2 = len(packet_2)

    if length_1 > length_2:
        return 1
    elif length_1 < length_2:
        return -1
    else:
        return 0


def read_packets(path) -> List[Packet]:
    packets = []
    for line in read_file(path):
        if line == '':
            continue
        else:
            packet = json.loads(line)
            packets.append(packet)

    return packets


def main():
    cwd = Path(__file__).parent

    packets = read_packets(cwd/'input')

    # task 1
    correct_order_index_sum = 0
    for pair_index, packet_index in enumerate(range(0, len(packets), 2), start=1):
        pair = packets[packet_index:packet_index + 2]
        if compare(*pair) == -1:
            # print('\n'.join(map(str, (pair_index, *pair))))
            correct_order_index_sum += pair_index
    print(correct_order_index_sum)

    # task 2
    divider_packets = [[[2]], [[6]]]
    sorted_packets = sorted(packets + divider_packets, key=cmp_to_key(compare))

    write_file(sorted_packets, cwd/'output')

    decoder_key = np.prod([sorted_packets.index(divider_packet) + 1 for divider_packet in divider_packets])
    print(decoder_key)


if __name__ == '__main__':
    main()
