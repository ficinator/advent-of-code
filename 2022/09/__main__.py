# task: https://adventofcode.com/2022/day/9
from enum import Enum
from pathlib import Path

import numpy as np

from utils.files import read_file


class Direction(Enum):
    R = (1, 0)
    D = (0, -1)
    L = (-1, 0)
    U = (0, 1)


class Rope:
    def __init__(self, n_knots: int = 2):
        self.knots = [np.zeros((2,), dtype=int) for _ in range(n_knots)]
        self.tail_positions = {(0, 0)}

    def __str__(self) -> str:
        return f"({', '.join(map(str, self.knots))})"

    def move(self, direction: Direction):
        # move head
        self.knots[0] += direction.value
        # move tails
        for i in range(1, len(self.knots)):
            diff = self.knots[i - 1] - self.knots[i]
            diff_abs = abs(diff)
            self.knots[i] += diff.clip(-1, 1) if any(diff_abs > 1) else (0, 0)
        self.tail_positions.add(tuple(self.knots[-1]))


def main():
    # task 1
    rope_1 = Rope()
    # task 2
    rope_2 = Rope(n_knots=10)
    for line in read_file(Path(__file__).parent/'input'):
        direction_str, distance = line.split()
        direction = Direction[direction_str]
        for _ in range(int(distance)):
            rope_1.move(direction)
            rope_2.move(direction)

    print(len(rope_1.tail_positions))
    print(len(rope_2.tail_positions))


if __name__ == '__main__':
    main()
