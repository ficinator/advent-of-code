# task: https://adventofcode.com/2022/day/8
from pathlib import Path
from typing import Tuple, List

import numpy as np

from utils.files import read_file


def read_forest() -> np.ndarray:
    forest = []
    for line in read_file(Path(__file__).parent/'input'):
        forest.append(list(line))
    return np.asarray(forest, dtype=int)


def get_trees_around(i: int, j: int, forest: np.ndarray, use_edge: bool = False) -> List[np.ndarray]:
    num_rows, num_cols = forest.shape
    trees_around = [
        # north
        np.flip(forest[0:i, j]),
        # east
        forest[i, j + 1:num_cols],
        # south
        forest[i + 1:num_rows, j],
        # west
        np.flip(forest[i, 0:j]),
    ]
    if use_edge:
        for trees_single_dir in trees_around:
            trees_single_dir[-1] = 9
    return trees_around


def is_tree_visible(i: int, j: int, forest: np.ndarray) -> bool:
    tree = forest[i, j]
    trees_around = get_trees_around(i, j, forest)
    return any(all(tree > trees_single_dir) for trees_single_dir in trees_around)


def get_tree_scenic_score(i: int, j: int, forest: np.ndarray) -> int:
    tree = forest[i, j]
    trees_around = get_trees_around(i, j, forest, use_edge=True)
    scores = [np.argmax(trees_single_dir >= tree) + 1 for trees_single_dir in trees_around]
    scenic_score = int(np.prod(scores))
    return scenic_score


def main():
    forest = read_forest()
    num_rows, num_cols = forest.shape
    # task 1
    num_visible_trees = 2 * num_rows + 2 * num_cols - 4
    # task 2
    max_scenic_score = 0

    for i in range(1, num_rows - 1):
        for j in range(1, num_cols - 1):
            is_visible = is_tree_visible(i, j, forest)
            num_visible_trees += is_visible

            scenic_score = get_tree_scenic_score(i, j, forest)
            if scenic_score > max_scenic_score:
                max_scenic_score = scenic_score

    # task 1
    print('# of visible trees:', num_visible_trees)
    # task 2
    print('max scenic score:', max_scenic_score)



if __name__ == '__main__':
    main()
