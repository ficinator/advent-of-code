# task: https://adventofcode.com/2022/day/11
from operator import attrgetter
from pathlib import Path
from typing import List, TypeVar

import numpy as np

from utils.files import read_file

T = TypeVar('T', int, np.ndarray)


class Monkey:
    def __init__(
            self,
            number: int,
            items: List[T],
            operation: str,
            divisor: int,
            monkey_num_true: int,
            monkey_num_false: int,
    ):
        self.number = number
        self.items: List[T] = items
        self.operation = operation
        self.divisor = divisor
        self.monkey_num_true = monkey_num_true
        self.monkey_num_false = monkey_num_false
        self.times_inspected = 0

    def __str__(self):
        return f'{self.number}: {self.times_inspected}, {self.operation}, {self.divisor}, {self.items}'

    def get_receiver(self, worry_level: T) -> int:
        test = (worry_level % self.divisor if isinstance(worry_level, int) else worry_level[self.number]) == 0
        return self.monkey_num_true if test else self.monkey_num_false

    def receive_item(self, item: T):
        self.items.append(item)
        # print(f'Monkey {self.number} received item {item} ({self.items})')


class MonkeyGroup:
    def __init__(self, monkeys: List[Monkey]):
        self.monkeys = monkeys

    def __str__(self) -> str:
        return '\n'.join(map(str, self.monkeys))

    @property
    def divisors(self) -> np.ndarray:
        return np.asarray([monkey.divisor for monkey in self.monkeys])

    @classmethod
    def from_file(cls, path: Path) -> 'MonkeyGroup':
        monkeys = []
        monkey_num = None
        items = None
        operation = None
        divisor = None
        monkey_num_true = None
        monkey_num_false = None
        for line in read_file(path):
            line = line.lstrip()
            if line.startswith('Monkey'):
                monkey_num = int(line[7:-1])
            elif line.startswith('Starting items:'):
                items = list(map(int, line[16:].split(', ')))
            elif line.startswith('Operation:'):
                operation = line[17:]
            elif line.startswith('Test:'):
                divisor = int(line[19:])
            elif line.startswith('If true:'):
                monkey_num_true = int(line[25:])
            elif line.startswith('If false:'):
                monkey_num_false = int(line[26:])
            elif line == '':
                monkey = Monkey(
                    number=monkey_num,
                    items=items,
                    operation=operation,
                    divisor=divisor,
                    monkey_num_true=monkey_num_true,
                    monkey_num_false=monkey_num_false,
                )
                monkeys.append(monkey)

        # append the last monkey
        monkey = Monkey(
            number=monkey_num,
            items=items,
            operation=operation,
            divisor=divisor,
            monkey_num_true=monkey_num_true,
            monkey_num_false=monkey_num_false,
        )
        monkeys.append(monkey)

        monkey_group = cls(monkeys)

        return monkey_group

    def round(self, lower_worry_level: bool = True):
        print(self)
        for monkey in self.monkeys:
            self.turn(monkey.number, lower_worry_level=lower_worry_level)

    def turn(self, monkey_num: int, lower_worry_level: bool = True):
        monkey = self.monkeys[monkey_num]
        while monkey.items:
            item = monkey.items.pop(0)
            monkey.times_inspected += 1
            worry_level = self.get_worry_level(
                item=item,
                operation=monkey.operation,
                lower_worry_level=lower_worry_level,
            )
            throw_to = monkey.get_receiver(worry_level)
            self.monkeys[throw_to].receive_item(worry_level)

    def get_worry_level(
            self,
            item: T,
            operation: str,
            lower_worry_level: bool = True,
    ) -> T:
        worry_level = eval(operation.replace('old', str(item) if isinstance(item, int) else f'np.as{repr(item)}'))
        if lower_worry_level:
            worry_level //= 3
        else:
            worry_level %= self.divisors
        return worry_level

    def get_monkey_business(self) -> int:
        times_inspected_sorted = sorted(map(attrgetter('times_inspected'), self.monkeys), reverse=True)
        return times_inspected_sorted[0] * times_inspected_sorted[1]


def main(n_rounds: int, lower_worry_level: bool = True):
    monkey_group = MonkeyGroup.from_file(Path(__file__).parent / 'input')

    for round_num in range(n_rounds):
        print(f'Round {round_num:2d}:')
        monkey_group.round(lower_worry_level=lower_worry_level)

    print('Final:')
    print(monkey_group)
    print('Monkey business:', monkey_group.get_monkey_business())


if __name__ == '__main__':
    # task 1
    main(n_rounds=20)
    # task 2
    main(n_rounds=10000, lower_worry_level=False)
