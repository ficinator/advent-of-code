# assignment https://adventofcode.com/2022/day/3
from pathlib import Path
from typing import Set, List

from utils.files import read_file

items = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
item_priorities = {item: i + 1 for i, item in enumerate(items)}
group_size = 3


def get_shared_item(content: str) -> str:
    half_size = len(content) // 2
    return set(content[:half_size]).intersection(content[half_size:]).pop()


def get_badge(group_contents: List[Set[str]]) -> str:
    return set.intersection(*group_contents).pop()


def main():
    shared_item_priority_sum = 0
    badge_priority_sum = 0
    group_contents = []
    for i, line in enumerate(read_file(Path(__file__).parent / 'input')):
        # task 1: find the shared items in the backpack
        shared_item = get_shared_item(line)
        shared_item_priority_sum += item_priorities[shared_item]

        # task 2: find the group's badge
        group_contents.append(set(line))
        if (i + 1) % group_size == 0:
            # the group is complete, find the badge
            badge = get_badge(group_contents)
            badge_priority_sum += item_priorities[badge]
            # reset the group contents
            group_contents = []
    # task 1
    print('Sum of shared item priorities:', shared_item_priority_sum)
    # task 2
    print('Sum of badge priorities:', badge_priority_sum)


if __name__ == '__main__':
    main()
