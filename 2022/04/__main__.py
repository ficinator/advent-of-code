from pathlib import Path
from typing import List, Tuple

from utils.files import read_file


def parse_range_str(sections_str: str) -> Tuple[int, int]:
    """ Parse the string representing the range.

    Args:
        sections_str: range string in format <start>-<end>, e.g. 5-7

    Returns:
        tuple (start, end + 1), e.g. (5, 8)
    """
    start, end = map(int, sections_str.split('-'))
    return start, end + 1


def does_overlap(range_1: Tuple[int, int], range_2: Tuple[int, int], fully: bool = False) -> bool:
    """ Whether the given ranges [fully] overlap.

    Args:
        range_1: half-open interval <start, end)
        range_2: half-open interval <start, end)
        fully: whether one of the ranges should be fully overlapping with the other

    Returns:
        True, if one of the ranges [fully] overlaps with the other
    """
    overlap = max(0, min(range_1[1], range_2[1]) - max(range_1[0], range_2[0]))
    if fully:
        return overlap == min(range_1[1] - range_1[0], range_2[1] - range_2[0])
    return overlap > 0


def main():
    n_sections_fully_overlap = 0
    n_sections_overlap = 0

    for line in read_file(Path(__file__).parent/'input'):
        range_1, range_2 = map(parse_range_str, line.split(','))
        fully_overlap = does_overlap(range_1, range_2, fully=True)
        overlap = does_overlap(range_1, range_2)
        print(range_1, range_2, fully_overlap, overlap)
        n_sections_fully_overlap += fully_overlap
        n_sections_overlap += overlap

    # task 1
    print('Fully overlap:', n_sections_fully_overlap)
    # task 2
    print('Overlap:', n_sections_overlap)


if __name__ == '__main__':
    main()
