# task: https://adventofcode.com/2022/day/6
from pathlib import Path

from utils.files import read_file


def get_marker_pos(seq: str, marker_length: int) -> int:
    for i in range(len(seq)):
        if len(set(seq[i:i + marker_length])) == marker_length:
            return i + marker_length


def main():
    seq = next(read_file(Path(__file__).parent/'input'))
    packet_marker_length = 4
    message_marker_length = 14

    packet_marker_pos = get_marker_pos(seq, marker_length=packet_marker_length)
    message_marker_pos = get_marker_pos(seq, marker_length=message_marker_length)

    print(packet_marker_pos, seq[packet_marker_pos - packet_marker_length:packet_marker_pos])
    print(message_marker_pos, seq[message_marker_pos - message_marker_length:message_marker_pos])


if __name__ == '__main__':
    main()
