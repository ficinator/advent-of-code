# assignment: https://adventofcode.com/2022/day/1

from pathlib import Path

from utils.files import read_file


if __name__ == '__main__':
    calories_per_elf = []
    curr_calories = 0
    top_n = 3

    for line in read_file(Path(__file__).parent/'input'):
        if line == '':
            calories_per_elf.append(curr_calories)
            curr_calories = 0
        else:
            curr_calories += int(line)

    sorted_calories_per_elf = sorted(calories_per_elf, reverse=True)

    print(sorted_calories_per_elf)
    # task 1
    print(f'Most calories: {sorted_calories_per_elf[0]}')
    # task 2
    print(f'Sum of top {top_n} calories: {sum(sorted_calories_per_elf[:top_n])}')
