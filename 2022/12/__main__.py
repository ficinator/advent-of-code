# task: https://adventofcode.com/2022/day/12
from operator import itemgetter
from pathlib import Path
from typing import Tuple, Set, List

import numpy as np

from utils.coords import Coords
from utils.files import read_file


class Map:
    def __init__(self, data: np.ndarray):
        self.data = data

    def __str__(self) -> str:
        return '\n'.join(''.join(row) for row in self.data)

    def get_coords_of_char(self, char: str) -> Coords:
        # noinspection PyTypeChecker
        return tuple(map(itemgetter(0), np.where(self.data == char)))

    @classmethod
    def read(cls, path: Path) -> 'Map':
        data = np.asarray([list(line) for line in read_file(path)])
        return cls(data=data)

    def find_shortest_path(self, start: str = 'S', end: str = 'E', descend: bool = False) -> List[Coords]:
        start_coords = self.get_coords_of_char(start)
        coords_to_explore = [start_coords]
        explored_coords = {start_coords}
        parents = {}
        shortest_path = []
        while coords_to_explore:
            x, y = coords_to_explore.pop(0)
            char = self.data[x, y]
            if char == end:
                shortest_path.append((x, y))
                break
            height = Map.get_height(char)
            for neighbor_coords in self.get_four_neighborhood(x, y):
                neighbor_char = self.data[neighbor_coords[0], neighbor_coords[1]]
                neighbor_height = Map.get_height(neighbor_char)
                is_neighbor_accessible = neighbor_height >= height - 1 if descend else neighbor_height <= height + 1
                if neighbor_coords in explored_coords or not is_neighbor_accessible:
                    continue
                coords_to_explore.append(neighbor_coords)
                explored_coords.add(neighbor_coords)
                parents[neighbor_coords[0], neighbor_coords[1]] = x, y

        while shortest_path[0] != start_coords:
            parent = parents[shortest_path[0]]
            shortest_path.insert(0, parent)

        return shortest_path

    def get_four_neighborhood(self, x: int, y: int) -> List[Coords]:
        max_x, max_y = self.data.shape
        neighbors_coords: List[Coords] = []
        if x > 0:
            # north
            neighbors_coords.append((x - 1, y))
        if x < max_x - 1:
            # south
            neighbors_coords.append((x + 1, y))
        if y > 0:
            # west
            neighbors_coords.append((x, y - 1))
        if y < max_y - 1:
            # east
            neighbors_coords.append((x, y + 1))
        return neighbors_coords

    @staticmethod
    def get_height(char: str) -> int:
        if char == 'S':
            char = 'a'
        elif char == 'E':
            char = 'z'
        return ord(char) - ord('a')

    def print_with_path(self, path: List[Coords]):
        for x, data_row in enumerate(self.data):
            for y, char in enumerate(data_row):
                color = 15 if (x, y) in path else Map.get_height(char) + 124
                print('\033[38;5;{}m{}'.format(color, char), end='')
            print('\033[39m')


def main():
    map = Map.read(Path(__file__).parent/'input')
    path_1 = map.find_shortest_path()
    map.print_with_path(path_1)

    print(len(path_1) - 1)

    path_2 = map.find_shortest_path(start='E', end='a', descend=True)
    map.print_with_path(path_2)

    print(len(path_2) - 1)


if __name__ == '__main__':
    main()
