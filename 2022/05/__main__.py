# task: https://adventofcode.com/2022/day/5
from copy import deepcopy
from pathlib import Path
from typing import List

from utils.files import read_file


def read_input():
    is_reading_crates = True
    stacks = []
    moves = []
    for line in read_file(Path(__file__).parent/'input'):
        if line == '':
            is_reading_crates = False
        elif is_reading_crates:
            # reading the crates
            crates = [line[i:i + 3].strip(' []') for i in range(0, len(line), 4)]
            for i, crate in enumerate(crates):
                if i >= len(stacks):
                    stacks.append([])
                if crate not in '123456789':
                    stacks[i].insert(0, crate)
        else:
            # reading the moves
            _, amount, _, from_stack, _, to_stack = line.split()
            moves.append((int(amount), int(from_stack) - 1, int(to_stack) - 1))

    return stacks, moves


def move_crates(
    stacks: List[List[str]],
    amount: int,
    from_stack: int,
    to_stack: int,
    move_multiple: bool = False
) -> None:
    crates = [stacks[from_stack].pop() for _ in range(amount)]
    stacks[to_stack] += crates[::-1] if move_multiple else crates


def main():
    stacks, moves = read_input()

    print(stacks)

    for crane_model, move_multiple in ((9000, False), (9001, True)):
        stacks_copy = deepcopy(stacks)
        for move in moves:
            move_crates(stacks_copy, *move, move_multiple=move_multiple)

        print(f'Crane {crane_model}:', ''.join(stack[-1] for stack in stacks_copy))


if __name__ == '__main__':
    main()
